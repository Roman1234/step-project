$(".our-services-list .our-services-item").eq(0).addClass("active");
$(".our-services .our-services-web-design").eq(0).addClass("active");
$('.our-services-list').on('click', ".our-services-item:not(.active)", function() {
    $(this).addClass("active");
    $(this).siblings().removeClass("active");
    $(this).closest(".our-services").find(".our-services-web-design").removeClass("active");
    const activeElementIndex = $(this).index();
    $(this).closest(".our-services").find(".our-services-web-design").eq(activeElementIndex).addClass("active");
});

const portfolioMenu = document.querySelectorAll('.our-work-item');
portfolioMenu.forEach(item => item.addEventListener('click', function() {
    const menu = this.closest('.our-work-list');
    const menuItem = menu.querySelector('.our-work-item.active-our-work');
    menuItem.classList.remove('active-our-work');
    this.classList.add('active-our-work');
    const selector = this.dataset.filter;
    const portfolio = document.querySelectorAll('.items-image');
    portfolio.forEach(item => item.style.display = 'none');
    const portfolioItem = document.querySelectorAll(`${selector}.items-image`);
    portfolioItem.forEach(elem => elem.style.display = '');
}));





const hideAuthors = document.querySelectorAll('.hide-authors');
const itemsImage = Array.from(document.querySelectorAll('.items-image'));
const applyHiddenClass = (elem, length = elem.length, count = 0) => {
    for (let i = count; i < length; i++) {
        elem[i].style.display = 'none';
    }
};

applyHiddenClass(hideAuthors);
applyHiddenClass(itemsImage, itemsImage.length, 12);

const miniCirclePhoto = document.querySelectorAll('.mini-circle-photo');
miniCirclePhoto.forEach((value) => value.addEventListener('click', showPeople));
function showPeople() {
    const miniCirclePhoto = Array.from(document.getElementsByClassName('mini-circle-photo'));
    const bigPeopleBlock = Array.from(document.getElementsByClassName('authors-main-block'));
    bigPeopleBlock.forEach((value) => value.style.display = 'none');
    const dataPeople = this.getAttribute('data-people');
    const dataBigBlock = bigPeopleBlock.filter((value) => {
        return value.getAttribute('data-people') === dataPeople;
    });
    dataBigBlock[0].style.display = '';
    miniCirclePhoto.forEach((value) => value.classList.remove('mini-circle-border'));
    this.classList.add('mini-circle-border');
}

const leftButton = document.querySelectorAll('.left');

const moveLeft = () => {
    const miniImages = Array.from(document.getElementsByClassName('mini-circle-photo'));
    const currentImageIndex = miniImages.findIndex((items) => items.classList.contains('mini-circle-border'));
    const currentImage = document.getElementsByClassName('mini-circle-border');
    let prevImageIndex = currentImageIndex - 1;
    let prevImage = miniImages[prevImageIndex];
    const bigPeopleBlock = Array.from(document.getElementsByClassName('authors-main-block'));

    bigPeopleBlock.forEach((items) => items.style.display = 'none');

    if (currentImageIndex === 0) {
        miniImages.forEach((items) => items.style.display = '');
        prevImage = miniImages[miniImages.length - 1];
        prevImageIndex = miniImages.length - 1;
    }
    currentImage[0].classList.remove('mini-circle-border');
    prevImage.classList.add('mini-circle-border');
    bigPeopleBlock[prevImageIndex].style.display = '';
};
leftButton[0].addEventListener('click', moveLeft);



const rightButton = document.querySelectorAll('.right');
const moveRight = () => {
    const miniImages = Array.from(document.getElementsByClassName('mini-circle-photo'));
    const currentImageIndex = miniImages.findIndex((items) => items.classList.contains('mini-circle-border'));
    const currentImage = document.getElementsByClassName('mini-circle-border');
    let nextImageIndex = currentImageIndex + 1;
    let nextImage = miniImages[nextImageIndex];
    const bigPeopleBlock = Array.from(document.getElementsByClassName('authors-main-block'));

    bigPeopleBlock.forEach((items) => items.style.display = 'none');

    if (currentImageIndex === (miniImages.length - 1)) {
        miniImages.forEach((items) => items.style.display = '');
        nextImage = miniImages[0];
        nextImageIndex = 0;
    }
    currentImage[0].classList.remove('mini-circle-border');
    nextImage.classList.add('mini-circle-border');
    bigPeopleBlock[nextImageIndex].style.display = '';
};
rightButton[0].addEventListener('click', moveRight);